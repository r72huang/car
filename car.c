#include "gpiolib_addr.h"
#include "gpiolib_reg.h"

#include <stdint.h>
#include <stdio.h>		//for the printf() function
#include <fcntl.h>
#include <linux/watchdog.h> 	//needed for the watchdog specific constants
#include <unistd.h> 		//needed for sleep
#include <sys/ioctl.h> 		//needed for the ioctl function
#include <stdlib.h> 		//for atoi
#include <time.h> 		//for time_t and the time() function
#include <sys/time.h>           //for gettimeofday()


int buttonStatus(GPIO_Handle gpio, int buttonNumber);
void setToOutput(GPIO_Handle gpio, int pinNumber);
int laserDiodeStatus(GPIO_Handle gpio, int diodeNumber);
void LEDOn(GPIO_Handle gpio, int pinNumber);
void LEDOff(GPIO_Handle gpio, int pinNumber);
void wheelForward(GPIO_Handle gpio, int in1, int in2);
void wheelBackward(GPIO_Handle gpio, int in1, int in2);
void wheelStop(GPIO_Handle gpio, int in1, int in2);
void testing(GPIO_Handle gpio);
void readConfig(FILE* configFile, int* timeout, char* logFileName, int* turnTime, int* runTime);
void getTime(char* buffer);

//Laser pin num: 4
//LED pin num: 17
//button pin num: 12
//EnableA: pin 25
//EnableB: pin 8

/* Motor 1 (right wheel):
In 1: pin 23
In 2: pin 24
*/

/* Motor 2 (left wheel):
In 1: pin 5
In 2: pin 6
*/

#define PRINT_MSG(file, time, programName, str) \
	do{ \
			fprintf(logFile, "%s : %s : %s", time, programName, str); \
			fflush(logFile); \
	}while(0)

#define PRINT_MSG2(file, time, programName, obstacle) \
	do{ \
			fprintf(logFile, "%s : %s : %s", time, programName, obstacle); \
			fflush(logFile); \
	}while(0)

int main(const int argc, const char* const argv[])
{

	char programName[4] = "car";	

	FILE* configFile;
	configFile = fopen("/home/pi/car.cfg", "r");

	//Output a warning message if the file cannot be openned
	if(!configFile)
	{
		perror("The config file could not be opened");
		return -1;
	}

	//Declare the variables that will be passed to the readConfig function
	int timeout;
	char logFileName[50];
	int turnTime;
	int runTime;

	//Call the readConfig function to read from the config file
	readConfig(configFile, &timeout, logFileName, &turnTime, &runTime);

	//Close the configFile now that we have finished reading from it
	fclose(configFile);

	//Create a new file pointer to point to the log file
	FILE* logFile;
	logFile = fopen(logFileName, "a");

	//Check that the file opens properly.
	if(!configFile)
	{
		perror("The log file could not be opened");
		return -1;
	}

	//Create a char array that will be used to hold the time values
	char time1[30];
	getTime(time1); ////////////////////////////////////////////////////////////

	//Initialize the GPIO pins
	GPIO_Handle gpio;
	gpio = gpiolib_init_gpio();

	//Initialize the GPIO pins and check if initialize correctly
	if(gpio == NULL)
	{
		perror("Could not initialize GPIO");
	}

	getTime(time1);
	//Log that the GPIO pins have been initialized
	PRINT_MSG(logFile, time, programName, "The GPIO pins have been initialized\n\n");

	setToOutput(gpio, 17);
	setToOutput(gpio, 25);
	setToOutput(gpio, 23);
	setToOutput(gpio, 24);
	setToOutput(gpio, 8);
	setToOutput(gpio, 5);
	setToOutput(gpio, 6);

	getTime(time1);
	PRINT_MSG(logFile, time1, programName, "Pins 17 25, 23, 24, 8, 5, 6 has been set to output\n\n");

	gpiolib_write_reg(gpio, GPSET(0), 1 << 25); //turn on enableA
	gpiolib_write_reg(gpio, GPSET(0), 1 << 8); //turn on enableB

	int watchdog;

	if ((watchdog = open("/dev/watchdog", O_RDWR | O_NOCTTY)) < 0) {
		printf("Error: Couldn't open watchdog device! %d\n", watchdog);
		return -1;
	} 

	//Get the current time
	getTime(time1);
	PRINT_MSG(logFile, time1, programName, "The Watchdog file has been opened\n\n");

	ioctl(watchdog, WDIOC_SETTIMEOUT, &timeout);
	
	getTime(time1);
	PRINT_MSG(logFile, time1, programName, "The Watchdog time limit has been set\n\n");

	ioctl(watchdog, WDIOC_GETTIMEOUT, &timeout);

	printf("The watchdog timeout is %d seconds.\n\n", timeout);


	time_t startTime = time(NULL);
	//int timeLimit = atoi(argv[1]);

	//int turnTime = 1000000; //1 sec to turn 90 deg
	int laserLight = 0;
	int obstacle = 0;

	LEDOn(gpio, 17);

	while((time(NULL) - startTime) < runTime)
	{	

		laserLight = laserDiodeStatus(gpio, 4);
		printf("laser %d\n", laserLight);

		getTime(time1);

		if(!laserLight){ //doesn't recieve light... OBSTACLE

			PRINT_MSG(logFile, time1, programName, "Obstacle Encountered\n\n");
			obstacle++;

			wheelStop(gpio, 23, 24); //both wheels off
			wheelStop(gpio, 5, 6); 

			sleep(1);
			getTime(time1);

			wheelBackward(gpio, 5, 6);
			wheelBackward(gpio, 23, 24);
			sleep(1);
			getTime(time1);
			//turn right
			wheelForward(gpio, 5, 6); //left wheel on
			wheelStop(gpio, 23, 24); //right wheel off
			sleep(turnTime); //time it takes for car to turn 90 degrees... CONFIG FILE
			getTime(time1);
			
		}
		else if(laserLight){ //recieves light... RUN FORWARD
		
			wheelForward(gpio, 23, 24); //both wheels forward
			wheelForward(gpio, 5, 6);
			usleep (50000);	
			getTime(time1);
		}

		wheelStop(gpio, 23, 24); //stop car
		wheelStop(gpio, 5, 6);
		usleep (50000);	

		ioctl(watchdog, WDIOC_KEEPALIVE, 0);
		getTime(time1);
		//Log that the Watchdog was kicked
		PRINT_MSG(logFile, time1, programName, "The Watchdog was kicked\n\n");
	} 

	wheelStop(gpio, 23, 24); //stop car
	wheelStop(gpio, 5, 6);

	//PRINT_MSG2(logFile, time1, programName, obstacle);

	//Writing a V to the watchdog file will disable to watchdog and prevent it from
	//resetting the system
	write(watchdog, "V", 1);
	getTime(time1);
	PRINT_MSG(logFile, time1, programName, "The Watchdog was disabled\n\n");

	close(watchdog);
	getTime(time1);
	PRINT_MSG(logFile, time1, programName, "The Watchdog was closed\n\n");


	//Free the GPIO now that the program is over.
	gpiolib_free_gpio(gpio);
	getTime(time1);
	PRINT_MSG(logFile, time1, programName, "The GPIO pins have been freed\n\n");
	return 0;
}

///////////////FUNCTIONS/////////////////


void setToOutput(GPIO_Handle gpio, int pinNumber){
	//Check that the gpio is functional
	if(gpio == NULL){
		printf("The GPIO has not been intitialized properly \n");
		return;
	}

	//Check that we are trying to set a valid pin number
	if(pinNumber < 2 || pinNumber > 27){
		printf("Not a valid pinNumer \n");
		return;
	}

	int registerNum = pinNumber / 10;

	int bitShift = (pinNumber % 10) * 3;

	uint32_t sel_reg = gpiolib_read_reg(gpio, GPFSEL(registerNum));
	sel_reg |= 1  << bitShift;
	gpiolib_write_reg(gpio, GPFSEL(registerNum), sel_reg);

}


int laserDiodeStatus(GPIO_Handle gpio, int laserPinNum){
	if(gpio == NULL){
		return -1;
	}

	uint32_t level_reg = gpiolib_read_reg(gpio, GPLEV(0));

	if(level_reg & (1 << laserPinNum)){ //laser recieved light
		return 1;
	}
	else{
		return 0;
	}	
	return 0;
}

void LEDOn(GPIO_Handle gpio, int pinNumber){
	if(gpio == NULL){
		printf("The GPIO has not been intitialized properly \n");
		return;
	}
	gpiolib_write_reg(gpio, GPSET(0), 1 << pinNumber);
}

void LEDOff(GPIO_Handle gpio, int pinNumber){
	if(gpio == NULL){
		printf("The GPIO has not been intitialized properly \n");
		return;
	}
	gpiolib_write_reg(gpio, GPCLR(0), 1 << pinNumber);
}

int buttonStatus(GPIO_Handle gpio, int buttonPinNum){
	if(gpio == NULL){
		return -1;
	}
	int pin_state = gpiolib_read_reg(gpio, GPLEV(0)) & (1 << buttonPinNum);
	return pin_state;
}

void wheelForward(GPIO_Handle gpio, int in1, int in2){
	if(gpio == NULL){
		printf("The GPIO has not been intitialized properly \n");
		return;
	}
	gpiolib_write_reg(gpio, GPSET(0), 1 << in1); //turn on in1
	gpiolib_write_reg(gpio, GPCLR(0), 1 << in2); //turn off in2
}

void wheelBackward(GPIO_Handle gpio, int in1, int in2){
	if(gpio == NULL){
		printf("The GPIO has not been intitialized properly \n");
		return;
	}
	gpiolib_write_reg(gpio, GPCLR(0), 1 << in1); //turn off in1
	gpiolib_write_reg(gpio, GPSET(0), 1 << in2); //turn on in2
}
void wheelStop(GPIO_Handle gpio, int in1, int in2){
	if(gpio == NULL){
		printf("The GPIO has not been intitialized properly \n");
		return;
	}
	gpiolib_write_reg(gpio, GPCLR(0), 1 << in1); //turn off in1
	gpiolib_write_reg(gpio, GPCLR(0), 1 << in2); //turn off in2
}


void testing(GPIO_Handle gpio){
	wheelForward(gpio, 5, 6); //left 
	wheelForward(gpio, 23, 24); //right
	usleep(4000000);

	wheelBackward(gpio, 5, 6); //left 
	wheelBackward(gpio, 23, 24); //right
	usleep(4000000);

	wheelForward(gpio, 5, 6); //left 
	wheelBackward(gpio, 23, 24); //right
	usleep(4000000);

	wheelForward(gpio, 23, 24); //right
	wheelBackward(gpio, 5, 6); //left 
	usleep(4000000);

	wheelStop(gpio, 23, 24); //stop car
	wheelStop(gpio, 5, 6);
	usleep(4000000);  

} 
	
void readConfig(FILE* configFile, int* timeout, char* logFileName, int* turnTime, int* runTime){
	//Loop counter
	//int i = 0;
	
	char buffer[255];

	*timeout = 0;
	*turnTime = 0;
	*runTime = 0;

	int input = 0;

	enum State{START, GET_WHITESPACE, GET_TIMEOUT, GET_LOGFILE, GET_NUMTURN, GET_RUNTIME, GET_COMMENT, DONE};
	enum State s = START;
	int get_timeout=0;
	int get_logfile=0;
	int get_numturn=0;
	int get_runtime=0;
	int i=0;
	while(s!=DONE)
	{
		switch(s)
		{
			case START:
			fgets(buffer, 255, configFile);
				if(buffer[i]=='#')
				{
					s= GET_COMMENT;
				}
				else if(buffer[i]==' ')
				{
					s=GET_WHITESPACE;
				}
				else if(buffer[i]=='W')
				{
					s=GET_TIMEOUT;
				
				}
				else if(buffer[i]=='L')
				{
					s=GET_LOGFILE;
					
				}
				else if(buffer[i]=='T')
				{
					s=GET_NUMTURN;
					
				}
				else if(buffer[i]=='R')
				{
					s=GET_RUNTIME;
					
				}
				break;
			
			case GET_COMMENT:
				printf("comment \n");

				if(fgets(buffer, 255, configFile)==NULL){
					s=DONE;
				}
				else
				{
				
				if(buffer[i]==' ')
				{
					s=GET_WHITESPACE;
				}
				else if(buffer[i]=='W')
				{
					s=GET_TIMEOUT;
	
				}
				else if(buffer[i]=='L')
				{
					s=GET_LOGFILE;
					
				}
				else if(buffer[i]=='T')
				{
					s=GET_NUMTURN;
					
				}
				else if(buffer[i]=='R')
				{
					s=GET_RUNTIME;
					
				}
				}
				break;
				
			case GET_WHITESPACE:
			printf("whitespace \n");
			

				if(fgets(buffer, 255, configFile)==NULL){
					s=DONE;
				}
				else
				{
				if(buffer[i]=='#')
				{
					s= GET_COMMENT;
				}
				else if(buffer[i]=='W')
				{
					s=GET_TIMEOUT;
					i++;
				}
				else if(buffer[i]=='L')
				{
					s=GET_LOGFILE;
					
				}
				else if(buffer[i]=='T')
				{
					s=GET_NUMTURN;
					
				}
				else if(buffer[i]=='R')
				{
					s=GET_RUNTIME;
					
				}
			}
				break;
				
			case GET_TIMEOUT:
			printf("timeout \n");
				get_timeout=1;
				while(buffer[i] != 0)
				{
				
					if(buffer[i] == '=')
					{
						while(buffer[i] != 0)
						{
							if(buffer[i] >= '0' && buffer[i] <= '9')
							{
								*timeout = (*timeout *10) + (buffer[i] - '0');
							}
							else if(buffer[i]== ' '){

							}
							else{
								get_timeout = 0;
							}
							i++;
						}
					}
					else
					{
						i++;
					}
				}
				i=0;

				if(fgets(buffer, 255, configFile)==NULL)
					s=DONE;
				else 
				{	
					fgets(buffer, 255, configFile);
					i=0;
					if(buffer[i]=='#')
					{
						s= GET_COMMENT;
											}
					else if(buffer[i]==' ')
					{
						s=GET_WHITESPACE;
						
					}
				
					else if(buffer[i]=='L')
					{
						s=GET_LOGFILE;
						
					}
					else if(buffer[i]=='T')
					{
						s=GET_NUMTURN;
					
					}
					else if(buffer[i]=='R')
					{
					s=GET_RUNTIME;
					
					}
				}
				break;
			case GET_LOGFILE:
			printf("logfile \n");
				get_logfile=1;
				while(buffer[i] != 0)
				{
					if(buffer[i] == '=')
					{
						int j = 0;
						while(buffer[i] != 0  && buffer[i] != '\n')
						{
							
							if(buffer[i] != ' ' && buffer[i] != '=')
							{
								logFileName[j] = buffer[i];
								j++;
							}
							i++;
						}
					}
					else
					{
						i++;
					}
				}
				i=0;
				if(fgets(buffer, 255, configFile)==NULL)
					s=DONE;
				else 
				{
					fgets(buffer, 255, configFile);
					i=0;
					if(buffer[i]=='#')
					{
						s= GET_COMMENT;
						
					}
					else if(buffer[i]==' ')
					{
						s=GET_WHITESPACE;
						
					}
					else if(buffer[i]=='T')
					{
						s=GET_NUMTURN;
					
					}
					else if(buffer[i]=='R')
					{
					s=GET_RUNTIME;
					
					}
				}
				break;
			case GET_NUMTURN:
			printf("numturn \n");
				get_numturn=1;
				while(buffer[i] != 0)
				{
					if(buffer[i] == '=' ) 
					{
					
						while(buffer[i] != 0)
						{
						
							if(buffer[i] >= '0' && buffer[i] <= '9')
							{
							
								*turnTime = (*turnTime *10) + (buffer[i] - '0');
							}
							else if(buffer[i]== ' '){

							}
							else{
								get_numturn = 0;
							}
							i++;
						}
					}
					else
					{
						i++;
					}
				}
				i=0;
				if(fgets(buffer, 255, configFile)==NULL)
					s=DONE;
				else 
				{
					i=0;
					fgets(buffer, 255, configFile);
					if(buffer[i]=='#')
					{
						s= GET_COMMENT;
						
					}
					else if(buffer[i]==' ')
					{
						s=GET_WHITESPACE;
						
					}
					else if(buffer[i]=='R')
					{
						s=GET_RUNTIME;
					
					}
				
				}
				break;

			case GET_RUNTIME:
				printf("runtime \n");
				get_runtime=1;
				while(buffer[i] != 0)
				{
					if(buffer[i] == '=' ) 
					{
					
						while(buffer[i] != 0)
						{
						
							if(buffer[i] >= '0' && buffer[i] <= '9')
							{
							
								*runTime = (*runTime *10) + (buffer[i] - '0');
							}
							else if(buffer[i]== ' '){

							}
							else{
								get_runtime = 0;
							}
							i++;
						}
					}
					else
					{
						i++;
					}
				}
				i=0;
				if(fgets(buffer, 255, configFile)==NULL)
					s=DONE;
				else 
				{
					i=0;
					fgets(buffer, 255, configFile);
					if(buffer[i]=='#')
					{
						s= GET_COMMENT;
						
					}
					else if(buffer[i]==' ')
					{
						s=GET_WHITESPACE;
						
					}
				
				}

				break;

			case DONE:
				return;
				break;
		}
	}

	if(get_timeout==0)
		*timeout=3;
	if(get_logfile==0){
		//*logFileName = {"/home/pi/car.log"};
		logFileName[0]='/';
		logFileName[1]='h';
		logFileName[2]='o';
		logFileName[3]='m';
		logFileName[4]='e';
		logFileName[5]='/';
		logFileName[6]='p';
		logFileName[7]='i';
		logFileName[8]='/';
		logFileName[9]='c';
		logFileName[10]='a';
		logFileName[11]='r';
		logFileName[12]='2';
		logFileName[13]='.';
		logFileName[14]='l';
		logFileName[15]='o';
		logFileName[16]='g';
	}
	if(get_numturn==0)
		*turnTime=1;
	if(get_runtime == 0)
		*runTime=20;


	printf("watchdog timeout %d \n", *timeout); ////////////////////////////////
	printf("log file %d \n", *logFileName);
	printf("turntime %d \n", *turnTime);
	printf("turntime %d \n", *runTime);

} 


//This function will get the current time using the gettimeofday function
void getTime(char* buffer)
{
  	struct timeval tv;

  	time_t curtime;

  	gettimeofday(&tv, NULL); 

  	curtime=tv.tv_sec;

  	strftime(buffer,30,"%m-%d-%Y  %T.",localtime(&curtime));

} 